# Basic packages

```
apt-get install vim openssh-server net-tools geany colordiff
```




# Install git

```
sudo apt-get install git
```

Configure user:

```
git config --global user.name "Auraham Camacho"
git config --global user.email "acamacho@tamps.cinvestav.mx"
git config --global core.editor vim
```

Check config:

```
git config --list
```



# Trash from command line

Install `trash-cli`

```
sudo apt-get install trash-cli
```
Create this script `/usr/local/bin/trash-rm`
```
sudo vim /usr/local/bin/trash-rm
```
And copy the following scritpt
```
#!/bin/bash
# command name: trash-rm
shopt -s extglob
recursive=1
declare -a cmd
((i = 0))
for f in "$@"
do
case "$f" in
(-*([fiIv])r*([fiIv])|-*([fiIv])R*([fiIv]))
tmp="${f//[rR]/}"
if [ -n "$tmp" ]
then
#echo "\$tmp == $tmp"
cmd[$i]="$tmp"
((i++))
fi
recursive=0 ;;
(--recursive) recursive=0 ;;
(*)
if [ $recursive != 0   -a  -d "$f" ]
then
echo "skipping directory: $f"
continue
else
cmd[$i]="$f"
((i++))
fi ;;
esac
done
trash-put "${cmd[@]}"
```
Then, make it executable
```
sudo chmod +x /usr/local/bin/trash-rm
```
FInally, add this alias in `~/.bash_aliases`
```
alias rm="trash-rm"
```

[webup8.org][http://www.webupd8.org/2010/02/make-rm-move-files-to-trash-instead-of.html]





# Configure `i3`


## Change status bar

Create a configuration file as follows:

```
mkdir ~/.config/i3status
cp /etc/i3status.conf ~/.config/i3status/config
```

Edit the following fields:

```
battery all {
        format = "%status %percentage %remaining"
        path = "/sys/class/power_supply/BAT%d/uevent"
        low_threshold = 30
        threshold_type = "time"
}

tztime local {
        format = "%Y-%b-%d %H:%M"
}
```

This will change the way battery and time are displayed, from:

```
FULL 100.00% | 2018-12-23 06:00:45
```

to:

```
FULL 100.00% | 2018-dic-23 06:00
```

When you battery is low, the display will change to color red.


## File manager

Install `pcmanfm` as file manager:

```
sudo apt-get install tango-icon-theme libpango1.0-0 pcmanfm
```

When you try to open a file in Nautilus and use the `backspace` key to go up in the directory structure in Ubuntu 18.10, you will notice that it is not possible. To restore this behavior, create this file:

```
vim ~/.config/gtk-3.0/gtk.css
```

```
@binding-set MyOwnFilechooserBindings
{
    bind "BackSpace" { "up-folder" () };
}

filechooser
{
    -gtk-key-bindings: MyOwnFilechooserBindings
}
```

Now, you can use `backspace` to navigate back through the directory structure.

[backspace key](https://askubuntu.com/questions/1046296/navigate-to-parent-folder-with-backspace-key-in-open-file-dialog)

To change the icons of `pcmanfm`, create this file:

```
vim ~/.gtkrc-2.0
```
```
gtk-icon-theme-name = "Humanity"
```


The icons are in this path:

```
/usr/share/icons/
```

[icons](https://askubuntu.com/questions/223399/how-to-get-icons-in-pcmanfm-ubuntu-12-10-with-only-fluxbox)


## Smooth font

Create a new file called `~/.Xresources` with this content:

```
vim ~/.Xresources
```

```
Xft.antialias: true
Xft.autohint: false
Xft.dpi: 96
Xft.hinting: true
Xft.hintstyle: hintslight
Xft.lcdfilter: lcddefault
Xft.rgba: rgb
```

Now, change the font of the status bar. Edit `~/.config/i3/config` as follows:

```
vim ~/.config/i3/config
```

and change this line

```
font pango:monospace 8
```

for this line:

```
font pango:DejaVu Sans Mono 10
```

Restart your computer. After restarting your computer, the font on the terminal (`gnome terminal`) and the status bar will be smoother.



# Python for deep learning

## 1. Basic packages


First, install Libatlas and BLAS:

```
sudo apt-get install build-essential python3-dev python3-setuptools libatlas-base-dev libatlas3-base
sudo apt-get install libopenblas-dev gfortran
```

----

**Note** I did not run this command because the paths of `libblas.so`, y `liblapack.so` are not the same as below:

```
sudo update-alternatives --set libblas.so.3 \
    /usr/lib/atlas-base/atlas/libblas.so.3
sudo update-alternatives --set liblapack.so.3 \
    /usr/lib/atlas-base/atlas/liblapack.so.3
```

-----

Then, install `virtualenv` and `virtualenvwrapper`:

```
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo python3 get-pip.py
sudo pip install virtualenv virtualenvwrapper
```

----

**Note** You could get this error:

```
Found existing installation: six 1.5.2
ERROR: Cannot uninstall 'six'. It is a distutils installed project and thus we cannot accurately determine which files belong to it which would lead to only a partial uninstall.
```

If so, use this command:

```
sudo pip install virtualenv virtualenvwrapper --ignore-installed six
```

[stackoverflow](<https://stackoverflow.com/questions/50954182/python-cannot-uninstall-six-while-pip-install-tld>)

----

Install packages for image/video tasks:

```
sudo apt-get install build-essential cmake unzip pkg-config
sudo apt-get install libjpeg-dev libtiff5-dev
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install libxvidcore-dev libx264-dev libgtk-3-dev
sudo apt-get install libhdf5-serial-dev graphviz
sudo apt-get install python-tk python3-tk python-imaging-tk
```

Headers for CUDA:

```
sudo apt-get install linux-image-generic linux-image-extra-virtual
sudo apt-get install linux-source linux-headers-generic
```

----

**Note** I had trouble to install these packages, so I omitted them:

```
libjasper-dev libpng12-dev
```

----



## 2. Create virtual environment

Add this snippet in `~/.bashrc`:

```
vim ~/.bashrc
```

```
# virtualenv and virtualenvwrapper
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source /usr/local/bin/virtualenvwrapper.sh
```

Reload

```
source ~/.bashrc
```

Create a new virtual environment called `dev` for python 3

```
mkvirtualenv keras -p python3
```

Verify that your prompt have `(keras)`

```(dev) auraham@rocket:~$ 
(keras) auraham@rocket:~$ 
```

Now, install numpy, scipy, matplotlib **inside** the virtual environment.

```
pip install numpy
pip install ipdb
pip install scipy
```

Install these packages **before** installing `matplotlib`

```
pip install cffi
pip install cairocffi
pip install pgi
pip install pygobject
```

Then, install `matplotlib` within the virtual environment

```
pip install matplotlib==1.5.0
```

Finally, install some additional packages

```
pip install --upgrade pandas
pip install jupyter
```

----

**Note** You could have this error message when using `pandas`:

```
ValueError: module functions cannot set METH_CLASS or METH_STATIC
```

According to [this post](https://github.com/pandas-dev/pandas/issues/19706), this error is due to we are using `backend: Gtk3Agg` in `matplotlibrc`. If you change the backend to `TkAgg`, the error is fixed. 

Another way to fix this error is to use `pandas==0.12.0` **along** with `matplotlib==1.5.0`.  

----

**Update** In `pandas==0.12.0` you will find these errors when trying to read/write a csv file:

```
TypeError: slice indices must be integers or None or have an __index__ method

TypeError: slice indices must be integers or None or have an __index__ method
```

To solve this issue, use `pandas==0.18.0`

-----

**Note** Use this package to render latex text with `backend: Cairo` and `text.usetex : True` in `~/.config/matplotlib/matplotlibrc`

```
sudo apt-get install dvipng
```

----

**Note** this is my configuration of `matplotlib` stored in `~/.config/matplotlib/matplotlibrc`:

```python
backend: Gtk3Agg
#text.usetex         : True    # this line is commented
```

----

**Note** You could get this error when plotting a text label with latex code in matplotlib:

```
RuntimeError: dvipng was not able to process the following file:
/home/auraham/.cache/matplotlib/tex.cache/9ba55e02336aca8757b175622c0a4d27.dvi
Here is the full report generated by dvipng: 
[this is empty]
```

To solve this issue, install `dvipng`:

```
(dev) auraham@rocket:~$ sudo apt-get install dvipng
```

[source](https://liuchangzju.wordpress.com/2015/12/05/python-unable-to-render-tex-in-matplotlib/) 

[Packages needed for matplotlib](https://www.pyimagesearch.com/2015/08/24/resolved-matplotlib-figures-not-showing-up-or-displaying/)

[Install ATLAS](http://scikit-learn.sourceforge.net/stable/install.html)

[PyImageSearch](https://www.pyimagesearch.com/2017/09/27/setting-up-ubuntu-16-04-cuda-gpu-for-deep-learning-with-python/)



## 3. Install CUDA 10

----

**Note** Not sure what version of CUDA to install? CUDA 10 is a good option for TensorFlow 2 and Keras at the time of this writing. You can check the requirements of TensorFlow 2 [here](https://www.tensorflow.org/install/gpu):

```
TensorFlow supports CUDA 10.0 (TensorFlow >= 1.13.0)
cuDNN >= 7.4.1
```

In addition, if you want to install PyTorch as well, we recommend to install CUDA 10.1 to get the latest version of PyTorch 1.3. Otherwise, you can still install PyTorch 1.2 with CUDA 10.

----

**Note** Add this repository to prevent issues when installing CUDA:

```
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt-get update
```

Otherwise, you may find these issues:

```
The following packages have unmet dependencies:
 xserver-xorg-video-nvidia-410 : Depends: xorg-video-abi-23 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-20 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-19 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-18 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-15 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-14 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-13 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-12 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-11 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-10 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-8 which is a virtual package and is not provided by any available package
 or
                                          xorg-video-abi-6.0 which is a virtual package and is not provided by any available package


```

[stackoverflow](https://askubuntu.com/questions/1118524/is-it-alright-that-the-nvidia-drivers-from-ppa-depend-on-xserver-xorg-core/1118586)

-----

Download `cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64.deb` from:

```
https://developer.nvidia.com/cuda-10.0-download-archive
```

Then, run these commands:

```
sudo dpkg -i cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64.deb
sudo apt-key add /var/cuda-repo-10-0-local-10.0.130-410.48/7fa2af80.pub
sudo apt-get update
sudo apt-get install cuda
```

This will take a few minutes. Now, some post-installation instructions from [here](https://developer.nvidia.com/cuda-80-ga2-download-archive). Add this to `.bashrc`:

```
# NVIDIA CUDA Toolkit
export PATH=/usr/local/cuda-10.0/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64/
```

Then, reload it as follows:

```
source ~/.bashrc
```

----

**Note** Run the following commands to test the installation:

```
cd /usr/local/cuda-10.0
cp -r samples/ ~/Desktop/
cd ~/Desktop/samples/1_Utilities/deviceQuery
make
./deviceQuery
```

You should see the following output:

```
./deviceQuery Starting...

 CUDA Device Query (Runtime API) version (CUDART static linking)

Detected 1 CUDA Capable device(s)

Device 0: "GeForce RTX 2060"
  CUDA Driver Version / Runtime Version          10.1 / 10.0
  CUDA Capability Major/Minor version number:    7.5
  Total amount of global memory:                 5931 MBytes (6219563008 bytes)
  (30) Multiprocessors, ( 64) CUDA Cores/MP:     1920 CUDA Cores
  GPU Max Clock rate:                            1710 MHz (1.71 GHz)
  Memory Clock rate:                             7001 Mhz
  Memory Bus Width:                              192-bit
  L2 Cache Size:                                 3145728 bytes
  Maximum Texture Dimension Size (x,y,z)         1D=(131072), 2D=(131072, 65536), 3D=(16384, 16384, 16384)
  Maximum Layered 1D Texture Size, (num) layers  1D=(32768), 2048 layers
  Maximum Layered 2D Texture Size, (num) layers  2D=(32768, 32768), 2048 layers
  Total amount of constant memory:               65536 bytes
  Total amount of shared memory per block:       49152 bytes
  Total number of registers available per block: 65536
  Warp size:                                     32
  Maximum number of threads per multiprocessor:  1024
  Maximum number of threads per block:           1024
  Max dimension size of a thread block (x,y,z): (1024, 1024, 64)
  Max dimension size of a grid size    (x,y,z): (2147483647, 65535, 65535)
  Maximum memory pitch:                          2147483647 bytes
  Texture alignment:                             512 bytes
  Concurrent copy and kernel execution:          Yes with 3 copy engine(s)
  Run time limit on kernels:                     Yes
  Integrated GPU sharing Host Memory:            No
  Support host page-locked memory mapping:       Yes
  Alignment requirement for Surfaces:            Yes
  Device has ECC support:                        Disabled
  Device supports Unified Addressing (UVA):      Yes
  Device supports Compute Preemption:            Yes
  Supports Cooperative Kernel Launch:            Yes
  Supports MultiDevice Co-op Kernel Launch:      Yes
  Device PCI Domain ID / Bus ID / location ID:   0 / 6 / 0
  Compute Mode:
     < Default (multiple host threads can use ::cudaSetDevice() with device simultaneously) >

deviceQuery, CUDA Driver = CUDART, CUDA Driver Version = 10.1, CUDA Runtime Version = 10.0, NumDevs = 1
Result = PASS
```
-----

## 4. Install cuDNN 7.6.5 for CUDA 10

Go to

```
https://developer.nvidia.com/rdp/cudnn-download
```

and download cuDNN v7.6.5

```
Download cuDNN v7.6.5 (November 5th, 2019), for CUDA 10.0
```

We use the `deb` packages for installation:

```
cuDNN Runtime Library for Ubuntu18.04 (Deb)
cuDNN Developer Library for Ubuntu18.04 (Deb)
```

Install the packages as follows:

```
sudo dpkg -i libcudnn7_7.6.5.32-1+cuda10.0_amd64.deb
sudo dpkg -i libcudnn7-dev_7.6.5.32-1+cuda10.0_amd64.deb
```



## 5. Install TensorFlow 2, Keras and PyTorch 

We will install both Tensorflow and Keras in the virtual environment created above, that is, `keras`. First, activate the virtual environment:

```
workon keras
```

Then, install some additional packages:

```python
# other packages for image processing
pip install pillow
pip install imutils h5py requests progressbar2
pip install scikit-learn scikit-image
```

Now, we are ready to install TensorFlow 2:

```
pip install --upgrade pip
pip install tensorflow-gpu
```

Check the installation from `ipython`:

```python
import tensorflow as tf                                                 
tf.__version__                                                           
```

You should see this:

```python
'2.0.0'
```

Now, install Keras:

```
pip install keras
```

and verify the installation as follows:

```python
import keras                                                                 
```

You should see this message:

```
Using TensorFlow backend.
```

Be sure that `~/.keras/keras.json` contains this configuration:

```json
{
    "floatx": "float32",
    "epsilon": 1e-07,
    "backend": "tensorflow",
    "image_data_format": "channels_last"
}
```

Make sure that `image_data_format` is set to `channels_last` and `backend` is `tensorflow`.

Now, we can install PyTorch. Be sure to select the version for CUDA 10:

```
# CUDA 10.0
pip install torch==1.2.0 torchvision==0.4.0 -f https://download.pytorch.org/whl/torch_stable.html
```

Finally, verify the installation from `ipython`:

```
In [1]: import torch                                                                 

In [2]: import torchvision                                                           

In [3]: torch.cuda.is_available()                                                    
Out[3]: True

In [4]: torch.__version__                                                            
Out[4]: '1.2.0+cu92'

In [5]: torchvision.__version__                                                      
Out[5]: '0.4.0+cu92'
```

That's all, folks!

----

**Note** This is the list of packages in the virtual environment`keras`:

```
pip freeze
```

```
absl-py==0.8.1
astor==0.8.0
astroid==2.3.3
attrs==19.3.0
backcall==0.1.0
bleach==3.1.0
cachetools==3.1.1
cairocffi==1.1.0
certifi==2019.9.11
cffi==1.13.2
chardet==3.0.4
cycler==0.10.0
decorator==4.4.1
defusedxml==0.6.0
entrypoints==0.3
gast==0.2.2
google-auth==1.7.1
google-auth-oauthlib==0.4.1
google-pasta==0.1.8
grpcio==1.25.0
h5py==2.10.0
idna==2.8
imageio==2.6.1
importlib-metadata==0.23
imutils==0.5.3
ipdb==0.12.2
ipykernel==5.1.3
ipython==7.9.0
ipython-genutils==0.2.0
ipywidgets==7.5.1
isort==4.3.21
jedi==0.15.1
Jinja2==2.10.3
joblib==0.14.0
jsonschema==3.1.1
jupyter==1.0.0
jupyter-client==5.3.4
jupyter-console==6.0.0
jupyter-core==4.6.1
Keras==2.3.1
Keras-Applications==1.0.8
Keras-Preprocessing==1.1.0
kiwisolver==1.1.0
lazy-object-proxy==1.4.3
Markdown==3.1.1
MarkupSafe==1.1.1
matplotlib==3.1.1
mccabe==0.6.1
mistune==0.8.4
more-itertools==7.2.0
nbconvert==5.6.1
nbformat==4.4.0
networkx==2.4
nose==1.3.7
notebook==6.0.2
numpy==1.17.4
oauthlib==3.1.0
opt-einsum==3.1.0
pandas==0.25.3
pandocfilters==1.4.2
parso==0.5.1
pexpect==4.7.0
pgi==0.0.11.2
pickleshare==0.7.5
Pillow==6.2.1
progressbar2==3.47.0
prometheus-client==0.7.1
prompt-toolkit==2.0.10
protobuf==3.10.0
ptyprocess==0.6.0
pyasn1==0.4.8
pyasn1-modules==0.2.7
pycairo==1.18.2
pycparser==2.19
Pygments==2.4.2
PyGObject==3.34.0
pylint==2.4.4
pyparsing==2.4.5
pyrsistent==0.15.5
python-dateutil==2.8.1
python-utils==2.3.0
pytz==2019.3
PyWavelets==1.1.1
PyYAML==5.1.2
pyzmq==18.1.1
qtconsole==4.5.5
requests==2.22.0
requests-oauthlib==1.3.0
rsa==4.0
scikit-image==0.16.2
scikit-learn==0.21.3
scipy==1.3.2
Send2Trash==1.5.0
six==1.13.0
tensorboard==2.0.1
tensorflow-estimator==2.0.1
tensorflow-gpu==2.0.0
termcolor==1.1.0
terminado==0.8.3
testpath==0.4.4
torch==1.2.0+cu92
torchvision==0.4.0+cu92
tornado==6.0.3
traitlets==4.3.3
typed-ast==1.4.0
urllib3==1.25.7
wcwidth==0.1.7
webencodings==0.5.1
Werkzeug==0.16.0
widgetsnbextension==3.5.1
wrapt==1.11.2
zipp==0.6.0
```

----



## 6. Additional packages for Jupyter

If you want to export notebooks as pdf, install the following packages:

```
apt-get install pandoc
apt-get install texlive
apt-get install texlive-xetex texlive-fonts-recommended texlive-generic-recommended
```

[source](https://pandoc.org/installing.html)

[source](https://nbconvert.readthedocs.io/en/latest/install.html#installing-tex)



## 7. Customize the style of Jupyter notebooks

Create `custom.css` inside `~/.jupyter/custom` with your css code.

```
cd ~/.jupyter/custom
vim custom.css
```

```
.container{
    width: 1440px;
}
```

If `~/.jupyter` does not exist, run `jupyter notebook --generate-config`.








